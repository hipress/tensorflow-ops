#include "terngrad.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "get_stream_tensorflow.h"

REGISTER_OP("Terngrad")
.Input("input1: float")
// .Input("input2: uint8")
.Output("output2: uint8")
.Attr("start: int")
.Attr("len: int")
.Attr("bitwidth: int")
.Attr("random: int")
.Attr("bypass: int")
.Doc(R"doc(
compress input data using Terngrad.
)doc");

REGISTER_OP("TerngradR")
.Input("input1: uint8")
.Input("input2: float")
.Output("output2: float")
.Attr("is_add_to: int")
.Attr("offset: int")
.Attr("bypass: int")
.Doc(R"doc(
decompress quantized data(uint8) to float using TerngradR.
)doc")
;

REGISTER_KERNEL_BUILDER(Name("Terngrad").Device(DEVICE_CPU), 
    TerngradOp<CPUDevice, CPUPolicy>);
REGISTER_KERNEL_BUILDER(Name("TerngradR").Device(DEVICE_CPU),
    TerngradROp<CPUDevice, CPUPolicy>);

