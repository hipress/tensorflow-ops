import tensorflow as tf
import os

bin_name = 'dgc.so'
parent_dir = os.path.join(os.path.dirname(__file__),'..')
bin_path = os.path.abspath(os.path.join(parent_dir, bin_name))
print(f'bin_path={bin_path}')

if __name__ == '__main__':
    tf.config.set_soft_device_placement(True)
    tf.debugging.set_log_device_placement(True)
    dgc_module = tf.load_op_library(bin_path)
    dgc = dgc_module.dgc
    dgc_r = dgc_module.dgc_r
    N = 1<<27
    with tf.device('/GPU:0'):
        G = tf.Variable(tf.random_uniform([N], minval=-2.0, maxval=2.0, dtype=tf.float32))
        d = dict()
        d[-1] = G
        for i in range(1000):
            Q = dgc(G,start=0,len=-1,s_percent=0.001, sample_rate=0.001, bypass=0)
            d[i] = dgc_r(Q, d[i-1], is_add_to=1, offset=0, bypass=0, tag=1)
        init_op = tf.initialize_all_variables()

    with tf.Session() as sess:
        sess.run(init_op)
        result = sess.run([d[2], d[999]])
        for v in result:
            print(v)