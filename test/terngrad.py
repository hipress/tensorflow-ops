import os
import tensorflow as tf
import logging
import colorful as cf
logging.basicConfig(format="%(filename)s[line:%(lineno)d]%(funcName)s\t%(message)s",level=logging.DEBUG)

tf.config.set_soft_device_placement(True)
tf.debugging.set_log_device_placement(True)

bin_name = 'terngrad.so'
parent_dir = os.path.join(os.path.dirname(__file__),'..')
bin_path = os.path.abspath(os.path.join(parent_dir, bin_name))
print(f'bin_path={bin_path}')


def verify():
    print('bin_path:',bin_path)
    terngrad_module = tf.load_op_library(bin_path)
    terngrad = terngrad_module.terngrad
    terngrad_r = terngrad_module.terngrad_r
    N = 1<<27
    bitwidth = 2
    random = 0
    import math 
    M = math.ceil(N/4)+10
    print("N={}\tM={}".format(N,M))
    num = 2
    with tf.device('/GPU:0'):
        G = tf.Variable(tf.random_uniform([N], minval=-2.0,maxval=2.0,dtype=tf.float32),name='G')
        Q = terngrad(
            G,
            start=0,
            len = -1,
            bitwidth=bitwidth,
            random=random,
            bypass=0
        )
        Q64 = terngrad(
            # G[:51380224],
            G,
            start = 0,
            len = 51380224,
            bitwidth=bitwidth,
            random=random,
            bypass=0
        )
        Q_size = tf.size(Q)
        Q64_size = tf.size(Q64)
        perN = (N+num-1)//num
        lastN = N - perN*(num-1)
        currNs = [lastN if i == num-1 else perN for i in range(num)]
        heads = [perN*i for i in range(num)]
        tails = [heads[i] + currNs[i] for i in range(num)]
        Qs = dict()
        Q_sizes = dict()
        for par in range(num):
            Qs[par] = terngrad(
                G[heads[par]:tails[par]],
                start = 0,
                len = -1,
                bitwidth=bitwidth,
                random=random,
                bypass=0
            )
            Q_sizes[par] = tf.size(Qs[par])
            logging.info(cf.red("par={}:\thead={}\ttail={}".format(par, heads[par], tails[par])))
            
        init_op = tf.initialize_all_variables()
    with tf.Session() as sess:
        sess.run(init_op)
        result = sess.run([Q,Q64,Q_size, Q64_size,Q_sizes[0],Q_sizes[1]])
        for v in result:
            print(v)

if __name__ == '__main__':
    verify()