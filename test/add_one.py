import os
import tensorflow as tf
tf.config.set_soft_device_placement(True)
tf.debugging.set_log_device_placement(True)

bin_name = 'cuda_op_kernel.so'
parent_dir = os.path.join(os.path.dirname(__file__),'..')
bin_path = os.path.abspath(os.path.join(parent_dir, bin_name))
print(f'bin_path={bin_path}')

def verify():
    add_one_module = tf.load_op_library(bin_path)
    add_one = add_one_module.add_one
    # with tf.device('/CPU:0'):
    #     input_data = tf.ones([3,4], tf.int32)
    # with tf.Session(''):
    #     print(input_data.eval())
    #     print("call add_one")
    #     output_data = add_one(input_data, K=3)
    #     print(output_data.eval())
    #     print(input_data.eval())
    gpus = tf.config.experimental.list_logical_devices('GPU')
    # with tf.device(gpus[0].name):
    with tf.device('/GPU:0'):
        input_data = tf.ones([3,4],tf.int32)
        input_data_var = tf.Variable(input_data)
        output_data = add_one(input_data_var, K=3)
        output_data2 = add_one(output_data, K=3)
        init_op = tf.initialize_all_variables()
    with tf.Session() as sess:
        sess.run(init_op)
        result = sess.run([output_data,input_data_var,output_data2])
        print(result[0])
        print(result[1])
        print(result[2])

    # with tf.Session(''):
    #     with tf.device(gpus[0].name):
    #         input_data = tf.ones([3,4], tf.int32)
    #     print(input_data.eval())
    #     print("call add_one")
    #     output_data = add_one(input_data, K=3)
    #     print(output_data.eval())
    #     print(input_data.eval())
        
if __name__ == '__main__':
    verify()