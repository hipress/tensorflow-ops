#include "generate_tensor.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "stdint.h"

REGISTER_OP("GenerateTensor")
.Attr("T: {float, uint8}")
.Input("input: T")
.Output("output: T")
.Doc(R"doc(
Allocate Tensor on specific device(CPU/GPU) with same shape as input.
)doc")
;

REGISTER_KERNEL_BUILDER(Name("GenerateTensor").Device(DEVICE_CPU).TypeConstraint<float>("T"),
    GenerateTensorOp<CPUDevice, float>);
REGISTER_KERNEL_BUILDER(Name("GenerateTensor").Device(DEVICE_CPU).TypeConstraint<uint8_t>("T"),
    GenerateTensorOp<CPUDevice, uint8_t>);