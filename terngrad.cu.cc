#include "terngrad.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "get_stream_tensorflow.h"

REGISTER_KERNEL_BUILDER(Name("Terngrad").Device(DEVICE_GPU),
    TerngradOp<GPUDevice,GPUPolicy>);
REGISTER_KERNEL_BUILDER(Name("TerngradR").Device(DEVICE_GPU),
    TerngradROp<GPUDevice,GPUPolicy>);