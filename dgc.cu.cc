#include "dgc.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "get_stream_tensorflow.h"

REGISTER_KERNEL_BUILDER(Name("Dgc").Device(DEVICE_GPU),
    DgcOp<GPUDevice,GPUPolicy>);
REGISTER_KERNEL_BUILDER(Name("DgcR").Device(DEVICE_GPU),
    DgcROp<GPUDevice,GPUPolicy>);