#include "generate_tensor.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "stdint.h"


// template<typename T>
// class GenerateTensorOp<GPUDevice, T>: public OpKernel{
//  public:
//     explicit GenerateTensorOp(OpKernelConstruction* context) : OpKernel(context){
//     }
//     void Compute(OpKernelContext* context) override{
//         printf("hello from seperate gpu version\n");
//         const Tensor& input_tensor = context->input(0);
//         Tensor* output_tensor = NULL;
//         OP_REQUIRES_OK(
//             context,
//             context->allocate_output(0, input_tensor.shape(),&output_tensor)
//         );
//         auto output=output_tensor->flat<T>();
//         auto input=input_tensor.flat<T>();
//         if (std::is_same<Device, CPUDevice>::value){
//             // printf("Generate tensor on CPU\n");
//             memcpy(output.data(), input.data(), sizeof(T)*output.size());
//         }
//         else{
//             cudaMemcpy(output.data(), input.data(), sizeof(T)*output.size(), cudaMemcpyDeviceToDevice);
//         }
//     }
// };

REGISTER_KERNEL_BUILDER(Name("GenerateTensor").Device(DEVICE_GPU).TypeConstraint<float>("T"),
    GenerateTensorOp<GPUDevice, float>);
REGISTER_KERNEL_BUILDER(Name("GenerateTensor").Device(DEVICE_GPU).TypeConstraint<uint8_t>("T"),
    GenerateTensorOp<GPUDevice, uint8_t>);