#ifndef _TENSORFLOW_GENERATE_TENSOR_H
#define _TENSORFLOW_GENERATE_TENSOR_H
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include <type_traits>
#include <string.h>
using namespace tensorflow;
typedef Eigen::ThreadPoolDevice CPUDevice;
typedef Eigen::GpuDevice GPUDevice;

template<typename Device, typename T>
class GenerateTensorOp: public OpKernel{
 public:
    explicit GenerateTensorOp(OpKernelConstruction* context) : OpKernel(context){
    }
    void Compute(OpKernelContext* context) override{
        const Tensor& input_tensor = context->input(0);
        context->set_output(0, input_tensor);
        // Tensor* output_tensor = NULL;
        // OP_REQUIRES_OK(
        //     context,
        //     context->allocate_output(0, input_tensor.shape(),&output_tensor)
        // );
        // auto output=output_tensor->flat<T>();
        // auto input=input_tensor.flat<T>();
        // if (std::is_same<Device, CPUDevice>::value){
        //     // printf("Generate tensor on CPU\n");
        //     memcpy(output.data(), input.data(), sizeof(T)*output.size());
        // }
        // else{
        //     // cudaMemcpy(output.data(), input.data(), sizeof(T)*output.size(), cudaMemcpyDeviceToDevice);
        //     // printf("output.size=%d\n", output.size());
        //     // printf("Generate tensor on GPU\n");
        // }
    }
};

#endif
