#ifndef _TENSORFLOW_TERNGRAD_H
#define _TENSORFLOW_TERNGRAD_H
// #define ZQDEBUG 1

// #include "absl/memory/memory.h"
// #include "absl/strings/str_cat.h"
// #include "absl/strings/string_view.h"
#include "tensorflow/core/framework/function.h"
#include "tensorflow/core/framework/graph_to_functiondef.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/lib/core/refcount.h"
#include "tensorflow/core/lib/strings/str_util.h"
#include "tensorflow/core/lib/strings/strcat.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/platform/mutex.h"
#include "tensorflow/core/platform/stream_executor.h"
#include "tensorflow/core/platform/thread_annotations.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/framework/shape_inference.h"
// #include "/home/togo/tensorflow/bazel-bin/external/local_config_cuda/cuda/_virtual_includes/cuda_headers_virtual/third_party/gpus/cuda/include/driver_types.h"
#include "bazel-bin/external/local_config_cuda/cuda/_virtual_includes/cuda_headers_virtual/third_party/gpus/cuda/include/driver_types.h"

#include <type_traits>
#include "get_stream_tensorflow.h"

#include "ANNOYMOUS_CPP_LIB/operate_memory/get_policy_general.h"
#include "ANNOYMOUS_CPP_LIB/gradient_compression_body/terngrad_body.h"
#include "ANNOYMOUS_CPP_LIB/likely.h"
using namespace annoymous_cpp_lib::operate_memory;
using namespace annoymous_cpp_lib::gradient_compression_body;

using namespace tensorflow;
typedef Eigen::ThreadPoolDevice CPUDevice;
typedef Eigen::GpuDevice GPUDevice;


#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/shape_inference.h"

#include <math.h>

// class TensorShape {
// public:
//   void AddDim(int64_t dim){
//       shape_.push_back(dim);
//   };
//   int dims() const {
//       return (int) shape_.size();
//   };
//   int64_t dim_size(int idx) const{
//       assert(idx>=0 && idx< shape_.size()); 
//       return shape_[idx];
//   };
//   int64_t num_elements() const{
//       int64_t result = 1;
//       for (auto dim: shape_){
//           result*=dim;
//       }
//       return result;
//   };

//   inline bool operator==(const TensorShape& rhs) const {
//     return shape_ == rhs.shape_;
//   }

//   inline bool operator!=(const TensorShape& rhs) const {
//     return shape_ != rhs.shape_;
//   }

// private:
//   std::vector<int64_t> shape_;
// };

// #define ZQDEBUG 0

template <typename Device, typename policy_t>
class TerngradOp: public OpKernel{
 public:
    explicit TerngradOp(OpKernelConstruction* context): OpKernel(context){
        OP_REQUIRES_OK(context, context->GetAttr("start", &start));
        OP_REQUIRES_OK(context, context->GetAttr("len", &len));
        OP_REQUIRES_OK(context, context->GetAttr("bitwidth", &bitwidth));
        OP_REQUIRES_OK(context, context->GetAttr("random", &random));
        OP_REQUIRES_OK(context, context->GetAttr("bypass", &bypass));
    }
    void Compute(OpKernelContext* context) override{
        #ifdef ZQDEBUG
        if (std::is_same<Device, CPUDevice>::value){
            printf("%s[LINE:%d, Time:%s]: Hello from CPU\n", __FILE__, __LINE__, __TIME__);
        }
        else{
            printf("%s[LINE:%d, Time:%s]: Hello from GPU\n", __FILE__, __LINE__, __TIME__);
        }
        #endif
        auto stream = get_stream<policy_t>::get(context);
        policy_t policy = get_policy<policy_t>::get(stream);
        bool mutable_flag=true;
        Tensor G_tensor = context->mutable_input(0,mutable_flag);
        auto G = G_tensor.flat<float>();
        auto G_size = len==-1? G.size():len;
        int data_per_byte = 8 / bitwidth;
        auto Q_size = 10 + (G_size + data_per_byte - 1) / data_per_byte;
        TensorShape Q_shape;
        Q_shape.AddDim(Q_size);
        Tensor* Q_tensor = NULL;
        context->allocate_output(0, Q_shape, &Q_tensor);
        auto Q = Q_tensor->flat<uint8_t>();
        #ifdef ZQDEBUG
            printf("\033[35m%s[LINE:%d, Time:%s]: name=", __FILE__, __LINE__, __TIME__);
            std::cout<<name()<<"\toutput_size="<<Q_size<<"\tinput_size="<<G_size;
            printf("\tstart=%d\tlen=%d",start,len);
            printf("\033[37m\n");
        #endif
        if (bypass){
            return;
        }
        // return;
        int ret = terngrad_body<policy_t>(
            G.data()+start,
            G_size,
            Q.data(),
            Q_size,
            bitwidth,
            random,
            policy,
            stream
        );
        if (unlikely(ret!=0)){
            printf("terngrad failed! Error Code = %d\n", ret);
            DCHECK(0);
        }
    }
 private:
    int start;
    int len;
    int bitwidth;
    int random;
    int bypass;
};

template <typename Device, typename policy_t>
class TerngradROp: public OpKernel{
 public:
    explicit TerngradROp(OpKernelConstruction* context): OpKernel(context){
        OP_REQUIRES_OK(context, context->GetAttr("is_add_to", &is_add_to));
        OP_REQUIRES_OK(context, context->GetAttr("offset", &offset));
        OP_REQUIRES_OK(context, context->GetAttr("bypass", &bypass));
    }
    void Compute(OpKernelContext* context) override{
        #ifdef ZQDEBUG
        if (std::is_same<Device, CPUDevice>::value){
            printf("%s[LINE:%d, Time:%s]: Hello from CPU\n", __FILE__, __LINE__, __TIME__);
        }
        else{
            printf("%s[LINE:%d, Time:%s]: Hello from GPU\n", __FILE__, __LINE__, __TIME__);
        }
        #endif
        auto stream = get_stream<policy_t>::get(context);
        policy_t policy = get_policy<policy_t>::get(stream);
        bool mutable_flag = true;
        Tensor Q_tensor = context->mutable_input(0,mutable_flag);
        Tensor G_tensor = context->mutable_input(1,mutable_flag);
        auto Q = Q_tensor.flat<uint8_t>();
        auto Q_size = Q.size();
        auto G = G_tensor.flat<float>();
        auto G_size = G.size();
        // auto true_G = G.data() + offset
        // auto true_G_size = G_size - offset
        context->set_output(0, G_tensor);
        if (bypass){
            return;
        }
        int ret = terngrad_r_body<policy_t>(
            G.data()+offset,
            G_size-offset,
            Q.data(),
            Q_size,
            is_add_to,
            policy,
            stream
        );
        if (unlikely(ret!=0)){
            printf("terngrad_r failed! Error Code = %d\n", ret);
            printf("terngrad_r: is_add_to=%d\n", is_add_to);
            DCHECK(0);
        }
    }
 private:
    int is_add_to;
    int offset;
    int bypass;
};

#endif