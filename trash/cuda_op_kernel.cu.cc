#define GOOGLE_CUDA 1
#define USE_MUTABLE 1
#if GOOGLE_CUDA  
#define EIGEN_USE_GPU

// #include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/common_shape_fns.h"

#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/platform/mutex.h"
#include "tensorflow/core/platform/stream_executor.h"
#include "tensorflow/core/platform/thread_annotations.h"
#include "tensorflow/core/platform/types.h"


using namespace tensorflow;
typedef Eigen::ThreadPoolDevice CPUDevice;
typedef Eigen::GpuDevice GPUDevice;

template <typename T>
#if USE_MUTABLE
__global__ void AddOneKernel(T* in, const int N, T* out) {
	in[0]=-2;
	int idx = blockIdx.x * N + threadIdx.x;
	out[idx] = in[idx] + 1;
}
#else
__global__ void AddOneKernel(const T* in, const int N, T* out) {
	int idx = blockIdx.x * N + threadIdx.x;
	out[idx] = in[idx] + 1;
}
#endif

template <typename T>
#if USE_MUTABLE
void AddOneKernelLauncher(T* in, const int batch_size, const int N, const int K, T* out) {
#else
void AddOneKernelLauncher(const T* in, const int batch_size, const int N, const int K, T* out) {
#endif
	printf("%d\n",__LINE__);
	AddOneKernel<T><<<batch_size, K>>>(in, N, out);
	cudaDeviceSynchronize();
}
#include <stdio.h>
template <typename Device, typename T>
class AddOneOp : public OpKernel {
public:
	explicit AddOneOp(OpKernelConstruction* context) : OpKernel(context) {
		OP_REQUIRES_OK(context, context->GetAttr("K", &K_));
	}

	void Compute(OpKernelContext* context) override {
		const cudaStream_t* stream = CHECK_NOTNULL(
      		reinterpret_cast<const cudaStream_t*>(context->op_device_context()
													->stream()
													->implementation()
													->GpuStreamMemberHack()));
        printf("hello from GPU\n");
		printf("stream=%d\n",stream);
		bool flag = true;
#if USE_MUTABLE
		Tensor input_tensor = context->mutable_input(0, flag);
#else
		const Tensor& input_tensor = context->input(0);
#endif
		auto input = input_tensor.flat<T>();
#if USE_MUTABLE
		// input.data()[0]=-2;
		// for (int i = 0; i < input.size(); i++){
		// 	printf("%d ", input.data()[i]);
		// }
		// printf("\n");
		int batch_size = input_tensor.shape().dim_size(0);
		int N = input.size() / batch_size;
#else
		const int batch_size = input_tensor.shape().dim_size(0);
		const int N = input.size() / batch_size;
#endif
		printf("%d\n",__LINE__);
		Tensor* output_tensor = NULL;
		OP_REQUIRES_OK(context, context->allocate_output(0, input_tensor.shape(), &output_tensor));
		auto output = output_tensor->flat<T>();
		OP_REQUIRES(context, K_>0 && K_ <= N,
			::tensorflow::errors::InvalidArgument("Invalid K value"));
		AddOneKernelLauncher<T>(input.data(), batch_size, N, K_, output.data());
	}
private:
	int K_;
};

REGISTER_KERNEL_BUILDER(Name("AddOne").Device(DEVICE_GPU).TypeConstraint<int>("T"),
                        AddOneOp<GPUDevice, int>);
REGISTER_KERNEL_BUILDER(Name("AddOne").Device(DEVICE_GPU).TypeConstraint<float>("T"),
                        AddOneOp<GPUDevice, float>);
REGISTER_KERNEL_BUILDER(Name("AddOne").Device(DEVICE_GPU).TypeConstraint<double>("T"),
                        AddOneOp<GPUDevice, double>);

#endif