#ifndef TENSORFLOW_GET_STREAM_TENSORFLOW_H
#define TENSORFLOW_GET_STREAM_TENSORFLOW_H


// #include "absl/memory/memory.h"
// #include "absl/strings/str_cat.h"
// #include "absl/strings/string_view.h"
// #include "tensorflow/core/framework/function.h"
// #include "tensorflow/core/framework/graph_to_functiondef.h"
// #include "tensorflow/core/framework/op.h"
// #include "tensorflow/core/framework/op_kernel.h"
// #include "tensorflow/core/lib/core/refcount.h"
// #include "tensorflow/core/lib/strings/str_util.h"
// #include "tensorflow/core/lib/strings/strcat.h"
// #include "tensorflow/core/platform/logging.h"
// #include "tensorflow/core/platform/mutex.h"
// #include "tensorflow/core/platform/stream_executor.h"
// #include "tensorflow/core/platform/thread_annotations.h"
// #include "tensorflow/core/platform/types.h"

#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "bazel-bin/external/local_config_cuda/cuda/_virtual_includes/cuda_headers_virtual/third_party/gpus/cuda/include/driver_types.h"
using namespace tensorflow;

#include <thrust/execution_policy.h>
#include <thrust/system/cuda/execution_policy.h>
#include <thrust/system/omp/execution_policy.h>
typedef thrust::cuda_cub::par_t::stream_attachment_type GPUPolicy;
typedef thrust::detail::host_t CPUPolicy;

#include "ANNOYMOUS_CPP_LIB/likely.h"


template<typename policy_t>
struct get_stream{
    static void* get(OpKernelContext* context){
        return NULL;
    }
};
template<>
struct get_stream<GPUPolicy>{
    static void* get(OpKernelContext* context){
        static cudaStream_t stream = NULL;
        if (unlikely(stream==NULL)){
            cudaStreamCreate(&stream);
        }
        return static_cast<void*>(stream);
    }
};

#endif