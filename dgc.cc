#include "dgc.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "get_stream_tensorflow.h"


REGISTER_OP("Dgc")
.Input("input1: float")
// .Input("input2: uint8")
.Output("output1: uint8")
.Attr("start: int")
.Attr("len: int")
.Attr("sample_rate: float")
.Attr("s_percent: float")
.Attr("bypass: int")
.Doc(R"doc(
compress input data using Dgc.
)doc");

REGISTER_OP("DgcR")
.Input("input1: uint8")
.Input("input2: float")
.Output("output2: float")
.Attr("is_add_to: int")
.Attr("offset: int")
.Attr("bypass: int")
.Attr("tag: int")
.Doc(R"doc(
decompress quantized data(uint8) to float using DgcR.
)doc")
;

REGISTER_KERNEL_BUILDER(Name("Dgc").Device(DEVICE_CPU), 
    DgcOp<CPUDevice, CPUPolicy>);
REGISTER_KERNEL_BUILDER(Name("DgcR").Device(DEVICE_CPU),
    DgcROp<CPUDevice, CPUPolicy>);

