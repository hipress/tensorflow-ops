.SECONDARY:	#keep files auto produced

BLACK = "\e[30;1m"
RED  =  "\e[31;1m"
GREEN = "\e[32;1m"
YELLOW = "\e[33;1m"
BLUE  = "\e[34;1m"
PURPLE = "\e[35;1m"
CYAN  = "\e[36;1m"
WHITE = "\e[37;1m"

# example
PATH_TENSORFLOW=~/tensorflow
PATH_TF_IN_PYTHON=~/anaconda3/lib/python3.6/site-packages/tensorflow


TF_CFLAGS=-I${PATH_TF_IN_PYTHON}/include
TF_LFLAGS=-L${PATH_TF_IN_PYTHON}
TF_LFLAGS=-L${PATH_TF_IN_PYTHON} -l:libtensorflow_framework.so
TF_IFLAGS=${PATH_TF_IN_PYTHON}/include
FLAGS=-I$(PATH_TENSORFLOW)


CUDA_DIR=/usr/local/cuda-10.1
NVCC=${CUDA_DIR}/bin/nvcc
CUDA_INCLUDE=${CUDA_DIR}/targets/x86_64-linux/include
CUDA_LIB=${CUDA_DIR}/targets/x86_64-linux/lib

ALL: $(subst .cu.cc,.so,$(wildcard *.cu.cc))
	@echo -e Stat: BUILD $(CYAN)$? $(WHITE)successfully

%_cpu.so: %.cc
	g++ $< -o $@ -std=c++11 -shared -fPIC ${TF_CFLAGS} -D_GLIBCXX_USE_CXX11_ABI=0 \
	${TF_LFLAGS} -O2 \
	-L${CUDA_LIB} -I${CUDA_INCLUDE} -lcudart ${FLAGS}

%_gpu.o: %.cu.cc
	${NVCC} $< -o $@ -std=c++11 -c ${TF_CFLAGS} -D_GLIBCXX_USE_CXX11_ABI=0 \
	${TF_LFLAGS} -D GOOGLE_CUDA=1 -x cu -Xcompiler -fPIC --expt-relaxed-constexpr -DNDEBUG -DGOOGLE_CUDA=1 \
	-L${CUDA_LIB} -I${CUDA_INCLUDE} -lcudart ${FLAGS}

%.so: %_gpu.o %_cpu.so
	g++ $? -o $@ -std=c++11 -shared -D_GLIBCXX_USE_CXX11_ABI=0 \
	-I${TF_IFLAGS} -I${TF_IFLAGS}/external/nsync/public ${TF_LFLAGS} -fPIC \
	-L${CUDA_LIB} -I${CUDA_INCLUDE} -lcudart ${FLAGS}
	@echo -e BUILD $(CYAN)$@ $(WHITE)successfully

ECHO:
	@echo TF_CFLAGS=${TF_CFLAGS}
	@echo TF_LFLAGS=${TF_LFLAGS}
	@echo TF_IFLAGS=${TF_IFLAGS}
	@echo FLAGS=${FLAGS}
	@echo CUDA_DIR=${CUDA_DIR}
	@echo NVCC=${NVCC}
	@echo CUDA_INCLUDE=${CUDA_INCLUDE}
	@echo CUDA_LIB=${CUDA_LIB}

color:
	@echo -e build $(CYAN) hahaha $(WHITE) ok

clean:
	rm *.so
	rm *.o
